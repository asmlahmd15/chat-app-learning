import 'package:chat_t_fb/controller/controller_chat.dart';
import 'package:chat_t_fb/serves/message_bubble.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatMassege extends StatefulWidget {
  final String roomId;

  const ChatMassege({Key? key, required this.roomId}) : super(key: key);

  @override
  State<ChatMassege> createState() => _ChatMassegeState();
}

class _ChatMassegeState extends State<ChatMassege> {
  final ChatController _chatController = Get.find();

  @override
  void initState() {
    super.initState();
    _chatController.getChatMessages(widget.roomId).listen((messages) {
      _chatController.initializeChatMessages(messages);
    }, onError: (error) {
      _chatController.setError(error.toString());
    });

    final currentUserId = FirebaseAuth.instance.currentUser!.uid;
    FirebaseFirestore.instance.collection('chat').doc(widget.roomId).update({
      'unreadCounter.$currentUserId': 0,
    });
  }



  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (_chatController.isLoading.value) {
        return Center(child: CircularProgressIndicator());
      } else if (_chatController.hasError.value) {
        return Center(child: Text('Error fetching messages'));
      } else {
        final localMessages = _chatController.localMessages;
        final chatMessages = _chatController.chatMessages;
        final allMessages = [...localMessages, ...chatMessages];
        return ListView.builder(
          itemCount: allMessages.length,
          reverse: true,
          itemBuilder: (con, index) {
            final chatMessage = allMessages[index];
            final nextMessage =
                index + 1 < allMessages.length ? allMessages[index + 1] : null;

            final currentMessageUserId = chatMessage['userId'];
            final nextMessageUserId =
                nextMessage != null ? nextMessage['userId'] : null;
            final bool nextUserIsSame =
                nextMessageUserId == currentMessageUserId;

            if (nextUserIsSame) {
              return MessageBubble.next(
                message: chatMessage['text'],
                isMe:
                    _chatController.authUser.value!.uid == currentMessageUserId,
              );
            } else {
              return MessageBubble.first(
                userImage: chatMessage['userImages'],
                username: chatMessage['userName'],
                message: chatMessage['text'],
                isMe:
                    _chatController.authUser.value!.uid == currentMessageUserId,
              );
            }
          },
        );
      }
    });
  }
}
