import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UserImagePacker extends StatefulWidget {
  const UserImagePacker({Key? key, required this.onPickedImages})
      : super(key: key);
  final void Function(File pickedImages) onPickedImages;

  @override
  State<UserImagePacker> createState() => _UserImagePackerState();
}

class _UserImagePackerState extends State<UserImagePacker> {
  File? _pickImageFile;

  void _pickImage() async {
    final XFile? packerImage = await ImagePicker().pickImage(
      source: ImageSource.camera,
      imageQuality: 50,
    );
    if (packerImage == null) {
      return;
    }
    setState(() {
      _pickImageFile = File(packerImage.path);
    });

    widget.onPickedImages(_pickImageFile!);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
          backgroundColor: Colors.grey,
          radius: 50,
          foregroundImage:
              _pickImageFile == null ? null : FileImage(_pickImageFile!),
        ),
        TextButton.icon(
          onPressed: _pickImage,
          icon: const Icon(Icons.image_outlined),
          label: const Text('add Image'),
        ),
      ],
    );
  }
}
