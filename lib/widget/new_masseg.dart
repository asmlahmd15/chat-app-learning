import 'package:chat_t_fb/controller/controller_chat.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewMassege extends StatefulWidget {
  final String roomId;
  const NewMassege({Key? key, required this.roomId}) : super(key: key);

  @override
  State<NewMassege> createState() => _NewMassegeState();
}

class _NewMassegeState extends State<NewMassege> {
  final ChatController _chatController = Get.put(ChatController());

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: Padding(
          padding: const EdgeInsets.all(16.0),
          child:Obx(() =>  TextField(
            decoration: const InputDecoration(
              labelText: 'أدخل الرسالة هنا',
              hintText: 'مثال: مرحباً بك في التطبيق!',
              border: OutlineInputBorder(),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.blue, width: 2.0),
              ),
            ),
            controller: TextEditingController(text: _chatController.messageText.value)
            ..selection = TextSelection.collapsed(
              offset: _chatController.messageText.value.length),

            onChanged: (value) {
              _chatController.updateMessageText(value);
            },
            style: const TextStyle(
              fontSize: 16.0,
              color: Colors.black,
            ),
          ),
        ),),),

        ElevatedButton.icon(onPressed: () {
          _chatController.sendMessage(widget.roomId);
        }, icon: const Icon(Icons.send), label: const Text(''))
      ],
    );
  }
}
