import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

enum ChatMessageStatus {
  sending,
  sent,
  delivered,
  read,
}

class ChatController extends GetxController {
  var messageText = ''.obs;
  RxBool isLoading = true.obs;
  RxBool hasError = false.obs;
  RxString errorMessage = ''.obs;

  var localMessages = <Map<String, dynamic>>[].obs;
  var authUser = FirebaseAuth.instance.currentUser.obs;

  var chatMessages = <Map<String, dynamic>>[].obs;

  void setError(String error) {
    hasError.value = true;
    errorMessage.value = error;
    isLoading.value = false;
  }

  void initializeChatMessages(List<Map<String, dynamic>> messages) {
    chatMessages.value = messages;
    isLoading.value = false;
  }



  Future<void> sendMessage(String roomId) async {
    try {
      if (messageText.value.trim().isEmpty) {
        print("The message is empty!");
        return;
      }

      final User user = FirebaseAuth.instance.currentUser!;
      final userData = await FirebaseFirestore.instance
          .collection('user')
          .doc(user.uid)
          .get();

      chatMessages.insert(
        0,
        {
          'text': messageText.value,
          'createAt': Timestamp.now(),
          'userId': user.uid,
          'userName': userData.data()!['username'],
          'userImages': userData.data()!['image_url'],
        },
      );

      await FirebaseFirestore.instance
          .collection('chat')
          .doc(roomId)
          .collection('messages')
          .add({
        'text': messageText.value,
        'createAt': Timestamp.now(),
        'userId': user.uid,
        'userName': userData.data()!['username'],
        'userImages': userData.data()!['image_url'],
      });

      await FirebaseFirestore.instance.collection('chat').doc(roomId).set({
        'lastMessageTimestamp': Timestamp.now(),
        // Assuming the recipient's ID is the second part of the roomId after splitting by '-'
        'unreadCounter.${roomId.split('-')[1]}': FieldValue.increment(1),
      }, SetOptions(merge: true));

      messageText.value = ''; // Clear the message
    } catch (error) {
      print("An error occurred: $error");
    }
  }

  Stream<List<Map<String, dynamic>>> getChatMessages(String roomId) {
    final user = FirebaseAuth.instance.currentUser;

    if (user == null) {
      return Stream.empty();
    }

    return FirebaseFirestore.instance
        .collection('chat')
        .doc(roomId)
        .collection('messages')
        .orderBy('createAt', descending: true)
        .snapshots()
        .map((snapshot) {
      final messages = snapshot.docs.map((doc) => doc.data()).toList();
      return messages;
    });
  }

  void updateMessageText(String newText) {
    messageText.value = newText;
    update();
  }
}
