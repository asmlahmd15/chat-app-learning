import 'package:chat_t_fb/chat.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AvailableContacts extends StatefulWidget {
  @override
  _AvailableContactsState createState() => _AvailableContactsState();
}

class _AvailableContactsState extends State<AvailableContacts> {
  final databaseReference = FirebaseDatabase.instance.reference();

  void onLineUser() {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      final userRef = databaseReference.child('presence').child(user.uid);
      userRef.set(true);
      userRef.onDisconnect().set(false);
    }
  }

  Future<bool> isUserOnline(String userId) async {
    final userRef = databaseReference.child('presence').child(userId);
    DatabaseEvent event = await userRef.once();
    return event.snapshot.value == true;
  }

  @override
  void initState() {
    onLineUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final currentUserId = FirebaseAuth.instance.currentUser!.uid;

    return Scaffold(
      appBar: AppBar(title: const Text('Available Contacts')),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('user').snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return const Center(child: Text('Error fetching contacts'));
          }

          final users = snapshot.data!.docs
              .where((doc) => doc.id != currentUserId)
              .map((doc) {
            final data = doc.data() as Map<String, dynamic>;

            return ChatUser(
              userId: doc.id,
              username: data['username'] ?? 'Unknown',
              userImage:
                  data['image_url'] ?? 'https://example.com/default_avatar.png',
            );
          }).toList();
          return ListView.builder(
            itemCount: users.length,
            itemBuilder: (context, index)   {
              final user = users[index];


              return FutureBuilder<bool>(
                future: isUserOnline(user.userId),
                builder: (context, snapshot){
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(user.userImage),
                      ),
                      title: Text(user.username),
                    );
                  }


                  final online = snapshot.data ?? false;
                 return ListTile(
                    leading:  Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (online)
                          Icon(Icons.circle, color: Colors.green, size: 12),
                        SizedBox(width: 5), // spacing
                        CircleAvatar(
                          backgroundImage: NetworkImage(user.userImage),
                        ),
                      ],
                    ),
                    title: Text(user.username),
                    onTap: () {
                      final selectedUserId = user.userId;
                      final List<String> ids = [currentUserId, selectedUserId];
                      ids.sort();
                      final roomId = ids.join('-');

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChatScreen(roomId: roomId),
                        ),
                      );
                    },
                  );


                }

              );
            },
          );
        },
      ),
    );
  }
}

class ChatUser {
  final String userId;
  final String username;
  final String userImage;

  ChatUser({
    required this.userId,
    required this.username,
    required this.userImage,
  });
}
