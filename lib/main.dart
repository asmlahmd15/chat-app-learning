
import 'package:chat_t_fb/auth.dart';
import 'package:chat_t_fb/available_contacts.dart';
import 'package:chat_t_fb/bindings_app.dart';
import 'package:chat_t_fb/chat.dart';
import 'package:chat_t_fb/firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
   // await CacheHelper.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: BindingsApp(),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: ((con,snap){
          if(snap.connectionState == ConnectionState.waiting){
           return const Scaffold(
             body: CircularProgressIndicator(),
           );
        }
           if(snap.hasData){
            return  AvailableContacts();
           }
          else {
            return  const AuthScreen();
          }
        }),),
    );
  }
}

