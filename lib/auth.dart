import 'dart:developer';
import 'dart:io';

import 'package:chat_t_fb/widget/user_mage_packer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  File? _selectedImages;

  final email= TextEditingController();
  final pass= TextEditingController();
  final userName= TextEditingController();

  final _firebase = FirebaseAuth.instance;


  Future<void> registerUser() async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email.text,
        password: pass.text,
      );
      User? user = userCredential.user;
      print("تم التسجيل بنجاح: ${user?.uid}");


      final Reference storegRef=FirebaseStorage.instance.
      ref().child('user_images').child('${userCredential.user!.uid}.jpg');

      await storegRef.putFile(_selectedImages!);
      final imageUrl= await storegRef.getDownloadURL();
      log(imageUrl);

     await FirebaseFirestore.instance.collection('user').doc(userCredential.user!.uid).set({
        'username':userName.text,
        'email':email.text,
        'image_url':imageUrl,
        'password':pass.text,
      });


    } catch (e) {
      print("حدث خطأ أثناء التسجيل: $e");

    }
  }


  Future<void> loginUser( ) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email.text,
        password: pass.text,
      );
      User? user = userCredential.user;
      print("تم تسجيل الدخول بنجاح: ${user!.uid}");

    } catch (e) {
      print("حدث خطأ أثناء تسجيل الدخول: $e");

    }
  }


  bool isLogin = true;

  void toggleAuthMode() {
    setState(() {
      isLogin = !isLogin;
    });
  }

  void submitForm()async {
    if (_formKey.currentState!.validate()) {
     await registerUser();

      // Valid form, perform authentication or registration logic here
      print("Email: ${email.text}");
      print("password : ${pass.text}");

    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(isLogin ? 'Login' : 'Register'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Center(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

               if(!isLogin) SizedBox(
                   width: 200,
                   height: 200,
                   child: UserImagePacker(
                     onPickedImages: (File pickedImages) {
                     setState(() {
                       _selectedImages=pickedImages;
                     });
                   },)),


                  if(!isLogin)
                    TextFormField(
                      controller: userName,
                      decoration: const InputDecoration(labelText: 'User Name'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Invalid User Name';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        userName.text = value!;
                      },
                    ),




                  TextFormField(
                    controller: email,
                    decoration: InputDecoration(labelText: 'Email'),
                    validator: (value) {
                      if (value!.isEmpty || !value.contains('@')) {
                        return 'Invalid email';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      email.text = value!;
                    },
                  ),
                  TextFormField(
                    controller: pass,
                    decoration: InputDecoration(labelText: 'Password'),
                    obscureText: true,
                    validator: (value) {
                      if (value!.isEmpty || value.length < 6) {
                        return 'Password must be at least 6 characters';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      pass.text = value!;
                    },
                  ),
                  SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: submitForm,
                    child: Text(isLogin ? 'Login' : 'Register'),
                  ),
                  TextButton(
                    onPressed: toggleAuthMode,
                    child: Text(isLogin ? 'Switch to Register' : 'Switch to Login'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
