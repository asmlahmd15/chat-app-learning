import 'package:chat_t_fb/widget/chat_masseg.dart';
import 'package:chat_t_fb/widget/new_masseg.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  final String roomId;

  const ChatScreen({Key? key, required this.roomId}) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App'),
        actions: [
          IconButton(
              onPressed: () {
                FirebaseAuth.instance.signOut();
              },
              icon: Icon(Icons.output))
        ],
      ),
      body: Column(
        children: [
          Expanded(
              child: ChatMassege(
            roomId: widget.roomId,
          )),
          NewMassege(roomId: widget.roomId)
        ],
      ),
    );
  }
}
